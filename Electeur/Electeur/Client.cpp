#undef UNICODE

#include <winsock2.h>
#include <iostream>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <string>

// Link avec ws2_32.lib
#pragma comment(lib, "ws2_32.lib")

using namespace std;

int __cdecl main(int argc, char **argv)
{
    WSADATA wsaData;
    SOCKET leSocket;// = INVALID_SOCKET;
    struct addrinfo *result = NULL,
                    *ptr = NULL,
                    hints;
	int iResult;
	char *msgType;
	char buf[128];
	std::map<string, string> candidats;

	//--------------------------------------------
    // InitialisATION de Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) 
	{
        printf("Erreur de WSAStartup: %d\n", iResult);
        return 1;
    }

	// On va creer le socket pour communiquer avec le serveur
	leSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (leSocket == INVALID_SOCKET) {
        printf("Erreur de socket(): %ld\n\n", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
		printf("Appuyez une touche pour finir\n");
		getchar();
        return 1;
	}

	//--------------------------------------------
	// On va chercher l'adresse du serveur en utilisant la fonction getaddrinfo.
    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_INET;        // Famille d'adresses
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;  // Protocole utilis� par le serveur

	char host[16] = "000.000.00.000";
	char port[5] = "5000";
	printf("Addresse du serveur: ");
	gets_s(host);
	printf("Port (5000 - 5050): ");
	gets_s(port);
	if (atoi(port) < 5000 || atoi(port) > 5050)
	{
		printf("Port invalide \n");
		return 1;
	}

	// getaddrinfo obtient l'adresse IP du host donn�
    iResult = getaddrinfo(host, port, &hints, &result);
    if ( iResult != 0 ) 
	{
        printf("Erreur de getaddrinfo: %d\n", iResult);
        WSACleanup();
        return 1;
    }

	//---------------------------------------------------------------------		
	//On parcours les adresses retournees jusqu'a trouver la premiere adresse IPV4
	while((result != NULL) &&(result->ai_family!=AF_INET))   
			 result = result->ai_next; 

	//-----------------------------------------
	if (((result == NULL) ||(result->ai_family!=AF_INET))) {
		freeaddrinfo(result);
		printf("Impossible de recuperer l'addresse \n");
        WSACleanup();
        return 1;
	}

	sockaddr_in *adresse;
	adresse=(struct sockaddr_in *) result->ai_addr;
	//----------------------------------------------------
	printf("Adresse du serveur %s : %s\n\n", host,inet_ntoa(adresse->sin_addr));
	printf("Connexion au serveur %s avec le port %s\n\n", inet_ntoa(adresse->sin_addr),port);
	
	// On va se connecter au serveur en utilisant l'adresse qui se trouve dans
	// la variable result.
	iResult = connect(leSocket, result->ai_addr, (int)(result->ai_addrlen));
	if (iResult == SOCKET_ERROR) 
	{
        printf("Impossible de se connecter au serveur %s sur le port %s \n", inet_ntoa(adresse->sin_addr),port);
        freeaddrinfo(result);
        WSACleanup();
		getchar();
        return 1;
	}

	printf("Connecte au serveur %s:%s \n", host, port);

	//-----------------------------
    // Requ�tes de la liste de candidats
	msgType = "REQ";
	if (send(leSocket, msgType, sizeof(msgType), 0) == SOCKET_ERROR)
	{
        printf("send error %d\n", WSAGetLastError());
        closesocket(leSocket);
        WSACleanup();
		getchar();
        return 1;
    }

	//------------------------------
	// R�ception des candidats
	bool endoflist = false;
	while (true)
	{
		iResult = recv(leSocket, buf, 128, 0);
		
		if (iResult > 0)
		{
			// 0 veut dire la fin de la liste
			if (buf[0] == '0')
			{
				break;
			}
			else
			{
				int sepIndex = 0;
				std::string str(buf);
				for (int y = 0; y < str.size(); y++)
				{
					if (str[y] == '_')	// Caract�re de s�paration
						sepIndex = y;
				}
				std::string index = str.substr(0, sepIndex);
				candidats[index] = str.substr(sepIndex + 1, str.size());;
			}
		}
		else if (iResult == 0) 
		{
			printf("Fermeture de la connection \n");
			closesocket(leSocket);
			WSACleanup();
			freeaddrinfo(result);
			return 0;
		}
		else 
		{
			printf("recv failed: %d\n", WSAGetLastError());
			closesocket(leSocket);
			WSACleanup();
			freeaddrinfo(result);
			return 0;
		}
	}

	// Afficher les candidats
	for (const auto iter : candidats)
	{
		cout << iter.first << ": " << iter.second << endl;
	}

	//Permettre a l'electeur d'entrer le nom de son choix.
	char choice[3];
	printf("Choix (entrez le numero du candidat): ");
	
	bool candidateFound = false;
	while(true)
	{
		gets_s(choice,3);
		if(atoi(choice) > 0 && atoi(choice) < candidats.size()+1)
		{
			break;
		}
		else
		{
			printf("Choix invalide \n");
			printf("Choix (entrez le numero du candidat): ");
		}
	}

	// On envoi le choix
	closesocket(leSocket);
	leSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (connect(leSocket, result->ai_addr, (int)(result->ai_addrlen)) != SOCKET_ERROR) 
	{
		string msg = string(choice);

		if (send(leSocket, msg.c_str(), sizeof(msg), 0) == SOCKET_ERROR)
		{
			printf("send error %d \n", WSAGetLastError());
			closesocket(leSocket);
			WSACleanup();
			return 1;
		}
		else cout << "Choisi " << msg.c_str() << endl;
		printf("Vote soumis \n");
	}
	else printf("Connection failed. \n");

    // cleanup
    closesocket(leSocket);
    WSACleanup();
	freeaddrinfo(result);
	getchar();
    return 0;
}