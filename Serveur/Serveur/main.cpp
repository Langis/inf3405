#undef UNICODE

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <strstream>
#include <time.h>
#include <sstream>
#include <windows.h>
#include <string>
#include <chrono>
#include <ctime>
#include <iomanip>

using namespace std;

// link with Ws2_32.lib
#pragma comment( lib, "ws2_32.lib" )

// External functions
extern DWORD WINAPI EchoHandler(void* sd_);
extern void DoSomething(char *src, char *dest);

// List of Winsock error constants mapped to an interpretation string.
// Note that this list must remain sorted by the error constants'
// values, because we do a binary search on the list when looking up
// items.
static struct ErrorEntry 
{
	int nID;
	const char* pcMessage;

	ErrorEntry(int id, const char* pc = 0) :
		nID(id),
		pcMessage(pc)
	{
	}

	bool operator<(const ErrorEntry& rhs) const
	{
		return nID < rhs.nID;
	}
} 
gaErrorList[] = {
	ErrorEntry(0, "No error"),
	ErrorEntry(WSAEINTR, "Interrupted system call"),
	ErrorEntry(WSAEBADF, "Bad file number"),
	ErrorEntry(WSAEACCES, "Permission denied"),
	ErrorEntry(WSAEFAULT, "Bad address"),
	ErrorEntry(WSAEINVAL, "Invalid argument"),
	ErrorEntry(WSAEMFILE, "Too many open sockets"),
	ErrorEntry(WSAEWOULDBLOCK, "Operation would block"),
	ErrorEntry(WSAEINPROGRESS, "Operation now in progress"),
	ErrorEntry(WSAEALREADY, "Operation already in progress"),
	ErrorEntry(WSAENOTSOCK, "Socket operation on non-socket"),
	ErrorEntry(WSAEDESTADDRREQ, "Destination address required"),
	ErrorEntry(WSAEMSGSIZE, "Message too long"),
	ErrorEntry(WSAEPROTOTYPE, "Protocol wrong type for socket"),
	ErrorEntry(WSAENOPROTOOPT, "Bad protocol option"),
	ErrorEntry(WSAEPROTONOSUPPORT, "Protocol not supported"),
	ErrorEntry(WSAESOCKTNOSUPPORT, "Socket type not supported"),
	ErrorEntry(WSAEOPNOTSUPP, "Operation not supported on socket"),
	ErrorEntry(WSAEPFNOSUPPORT, "Protocol family not supported"),
	ErrorEntry(WSAEAFNOSUPPORT, "Address family not supported"),
	ErrorEntry(WSAEADDRINUSE, "Address already in use"),
	ErrorEntry(WSAEADDRNOTAVAIL, "Can't assign requested address"),
	ErrorEntry(WSAENETDOWN, "Network is down"),
	ErrorEntry(WSAENETUNREACH, "Network is unreachable"),
	ErrorEntry(WSAENETRESET, "Net connection reset"),
	ErrorEntry(WSAECONNABORTED, "Software caused connection abort"),
	ErrorEntry(WSAECONNRESET, "Connection reset by peer"),
	ErrorEntry(WSAENOBUFS, "No buffer space available"),
	ErrorEntry(WSAEISCONN, "Socket is already connected"),
	ErrorEntry(WSAENOTCONN, "Socket is not connected"),
	ErrorEntry(WSAESHUTDOWN, "Can't send after socket shutdown"),
	ErrorEntry(WSAETOOMANYREFS, "Too many references, can't splice"),
	ErrorEntry(WSAETIMEDOUT, "Connection timed out"),
	ErrorEntry(WSAECONNREFUSED, "Connection refused"),
	ErrorEntry(WSAELOOP, "Too many levels of symbolic links"),
	ErrorEntry(WSAENAMETOOLONG, "File name too long"),
	ErrorEntry(WSAEHOSTDOWN, "Host is down"),
	ErrorEntry(WSAEHOSTUNREACH, "No route to host"),
	ErrorEntry(WSAENOTEMPTY, "Directory not empty"),
	ErrorEntry(WSAEPROCLIM, "Too many processes"),
	ErrorEntry(WSAEUSERS, "Too many users"),
	ErrorEntry(WSAEDQUOT, "Disc quota exceeded"),
	ErrorEntry(WSAESTALE, "Stale NFS file handle"),
	ErrorEntry(WSAEREMOTE, "Too many levels of remote in path"),
	ErrorEntry(WSASYSNOTREADY, "Network system is unavailable"),
	ErrorEntry(WSAVERNOTSUPPORTED, "Winsock version out of range"),
	ErrorEntry(WSANOTINITIALISED, "WSAStartup not yet called"),
	ErrorEntry(WSAEDISCON, "Graceful shutdown in progress"),
	ErrorEntry(WSAHOST_NOT_FOUND, "Host not found"),
	ErrorEntry(WSANO_DATA, "No host data of that type was found")
};
const int kNumMessages = sizeof(gaErrorList) / sizeof(ErrorEntry);


//// WSAGetLastErrorMessage ////////////////////////////////////////////
// A function similar in spirit to Unix's perror() that tacks a canned 
// interpretation of the value of WSAGetLastError() onto the end of a
// passed string, separated by a ": ".  Generally, you should implement
// smarter error handling than this, but for default cases and simple
// programs, this function is sufficient.
//
// This function returns a pointer to an internal static buffer, so you
// must copy the data from this function before you call it again.  It
// follows that this function is also not thread-safe.
const char* WSAGetLastErrorMessage(const char* pcMessagePrefix, int nErrorID = 0)
{
	// Build basic error string
	static char acErrorBuffer[256];
	ostrstream outs(acErrorBuffer, sizeof(acErrorBuffer));
	outs << pcMessagePrefix << ": ";

	// Tack appropriate canned message onto end of supplied message 
	// prefix. Note that we do a binary search here: gaErrorList must be
	// sorted by the error constant's value.
	ErrorEntry* pEnd = gaErrorList + kNumMessages;
	ErrorEntry Target(nErrorID ? nErrorID : WSAGetLastError());
	ErrorEntry* it = lower_bound(gaErrorList, pEnd, Target);
	if ((it != pEnd) && (it->nID == Target.nID)) {
		outs << it->pcMessage;
	}
	else {
		// Didn't find error in list, so make up a generic one
		outs << "unknown error";
	}
	outs << " (" << Target.nID << ")";

	// Finish error message off and return it.
	outs << ends;
	acErrorBuffer[sizeof(acErrorBuffer)-1] = '\0';
	return acErrorBuffer;
}

#define NB_SOCKETS 51

static addrinfo hints;
static vector<string> participants;
static vector<int> votes;
static vector<string> voters;
void msgHandler(char * buf, SOCKET sd)
{
	cout << "Received " << buf << endl;

	if (strcmp(buf, "REQ") == 0)
	{
		// We send two messages - one for the size of the name table and one for the table itself
		for (int i = 0; i < participants.size(); i++)
		{
			string msg = to_string(i + 1) + "_" + participants[i];

			if (send(sd, msg.c_str(), 128, 0) == SOCKET_ERROR)
				cout << "Send " << msg << " failed" << endl;
			else cout << msg << " sent." << endl;
		}
		if (send(sd, "0", 128, 0) == SOCKET_ERROR)
			cout << "Send 0 failed" << endl;
	}
	else
	{
		int val;
		sscanf_s(buf, "%d", &val);

		if (val-1 >= 0 && val-1 < participants.size()) {
			votes[val-1] += 1;
			cout << participants[val-1] << " " << votes[val-1] << endl;
		}
	}
}
DWORD WINAPI threadFunc(void * data) {
	SOCKET mySocket = socket(hints.ai_family, hints.ai_socktype, IPPROTO_TCP);

	int port = (int)(data);
	// printf("Opening port %d\n", port);

	if (mySocket != INVALID_SOCKET) {
		addrinfo * serverInfo;
		if (getaddrinfo(NULL, to_string(port).c_str(), &hints, &serverInfo) == 0) {
			if (bind(mySocket, serverInfo->ai_addr, serverInfo->ai_addrlen) != SOCKET_ERROR) {
				if (listen(mySocket, 30) != SOCKET_ERROR) {
					while (true) {
						sockaddr_in their_addr;
						socklen_t addr_size = sizeof(sockaddr);
						SOCKET acceptSocket = accept(mySocket, (sockaddr*)&their_addr, &addr_size);
						if (acceptSocket != INVALID_SOCKET)
						{
							printf("Connection received on port %d\n", port);
							char* buf = new char[3];
							if (recv(acceptSocket, buf, sizeof(buf), 0) != SOCKET_ERROR)
								msgHandler(buf, acceptSocket);
							else printf("recv failed %d\n", port);
						}
						else printf("accept error with port %d\n", port);
						closesocket(acceptSocket);
					}
				}
				else printf("listen error with port %d\n", port);
			}
			else printf("bind error with port %d\n", port);
		}
		else printf("getaddrinfo error with port %d\n", port);
	}
	else printf("socket error with port %d\n", port);

	return 0;
}

int main(void)
{
	//----------------------
	// Load Candidats
	// string path;
	// cout << "Fichier candidat : ";
	// cin >> path;
	ifstream in("candidats.txt");
	string line;
	while (getline(in, line))
	{
		participants.push_back(line);
		votes.push_back(0);

		cout << line << endl;
	}

	ifstream results("voteResults.txt");
	string entry;
	int index = 0;
	while (getline(results, entry))
	{
		size_t found = entry.find(":");
		int previousVoteCount = atoi(entry.substr(found+1, entry.size() - found).c_str());
		votes[index] = previousVoteCount;
		++index;
	}

	in.close();

	if (participants.empty())
	{
		cout << "candidats.txt not found." << endl;
		return 1;
	}
	else cout << endl << "candidats.txt loaded." << endl;

	memset(&hints, 0, sizeof(hints));	// empty struct
	hints.ai_family = AF_INET;			// IPv4
	hints.ai_socktype = SOCK_STREAM;	// TCP
	hints.ai_flags = AI_PASSIVE;		// auto fill ip

	float duration;
	printf("Duree du vote (en secondes) ");
	cin >> duration;

	// Initialize Winsock.
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) 
	{
		cerr << "Error at WSAStartup()\n" << endl;
		return 1;
	}

	int ports[NB_SOCKETS];
	HANDLE hThreadArray[NB_SOCKETS];

	// Initialise all threads
	cout << "Initialising ports 5000-5050...";
	for (int i = 0; i < NB_SOCKETS; i++)
	{
		ports[i] = 5000 + i;
		hThreadArray[i] = CreateThread(NULL, 0, threadFunc, (void*)ports[i], 0, NULL);

		if (hThreadArray[i] == NULL)
			printf("Port %d thread fail\n", ports[i]);
	}
	cout << "Done." << endl;

	WaitForMultipleObjects(NB_SOCKETS, hThreadArray, TRUE, duration * 1000);

	cout << endl << "RESULTATS========================================" << endl;
	string voteCountName = "voteResults.txt";
	ofstream voteCount;
	voteCount.open(voteCountName);
	for (int i = 0; i < participants.size(); i++) 
	{
		cout << participants[i] << " : " << votes[i] << endl;
		voteCount << participants[i] << " : " << votes[i] << endl;
	}
	voteCount.close();

	//Write vote log
	string userLogName = "userLog.txt";
	ofstream userLog;
	userLog.open(userLogName, std::ios::app);
	for (int i = 0; i < voters.size(); i++)
	{
		userLog << voters.at(i) << endl;
	}
	voteCount.close();
	
	////----------------------
	//// Get local IP Address and socket

	//int status;
	//// struct addrinfo hints;

	//fd_set readFDs;
	//FD_ZERO(&readFDs);

	////----------------------
	//// Create 51 sockets since we listen on port 5000 to 5050
	//SOCKET * ServerSockets = new SOCKET[nbSockets];
	//addrinfo ** ServerInfos = new addrinfo*[nbSockets];	// Array of structs
	//for (int i = 0; i < nbSockets; i++)
	//{
	//	// Create a socket
	//	ServerSockets[i] = socket(hints.ai_family, hints.ai_socktype, IPPROTO_TCP);
	//	if (ServerSockets[i] == INVALID_SOCKET)
	//	{
	//		cerr << WSAGetLastErrorMessage("Error at socket()") << endl;
	//		cleanup(ServerSockets, nbSockets);
	//		return 1;
	//	}
	//	// Create a servinfo to the right port
	//	if ((status = getaddrinfo(NULL, to_string(5000 + i).c_str(), &hints, &ServerInfos[i])) != 0)
	//	{
	//		fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
	//		cleanup(ServerSockets, nbSockets);
	//		return 1;
	//	}
	//	// bind it to the port we passed in to getaddrinfo()
	//	if (bind(ServerSockets[i], ServerInfos[i]->ai_addr, ServerInfos[i]->ai_addrlen) == SOCKET_ERROR)
	//	{
	//		cerr << WSAGetLastErrorMessage("bind() failed.") << endl;
	//		cleanup(ServerSockets, nbSockets);
	//		return 1;
	//	}
	//	// Listen for incoming connection requests on the new socket
	//	if (listen(ServerSockets[i], 30) == SOCKET_ERROR)
	//	{
	//		cerr << WSAGetLastErrorMessage("Error listening on socket.") << endl;
	//		cleanup(ServerSockets, nbSockets);
	//		return 1;
	//	}
	//	FD_SET(ServerSockets[i], &readFDs);
	//	maxFD = max(maxFD, ServerSockets[i]);
	//	
	//}

	//while (true) 
	//{
	//	printf("En attente des connections des clients sur les ports 5000-5050\n");

	//	if (select(maxFD+1, &readFDs, NULL, NULL, NULL) < 0)
	//	{
	//		perror("select error");
	//		return 1;
	//	}

	//	for (int i = 0; i < nbSockets; ++i)
	//	{
	//		if (FD_ISSET(ServerSockets[i], &readFDs))
	//		{
	//			sockaddr_in their_addr;
	//			socklen_t addr_size = sizeof(sockaddr);
	//			SOCKET acceptSocket = accept(ServerSockets[i], (sockaddr*)&their_addr, &addr_size);
	//			if (acceptSocket != INVALID_SOCKET) 
	//			{
	//				cout << "Connection acceptee De : " << inet_ntoa(their_addr.sin_addr) << ":" << their_addr.sin_port << "." << endl;
	//				char* buf = new char[3];
	//				if (recv(acceptSocket, buf, sizeof(buf), 0) != SOCKET_ERROR)
	//					msgHandler(buf, acceptSocket);
	//				else printf("recv failed %d \n", 5000 + i);
	//			}
	//			else printf("accept error");
	//			closesocket(acceptSocket);
	//		}
	//	}

	//	// sockaddr_in their_addr;
	//	// socklen_t addr_size = sizeof(sockaddr);
	//	// int acceptSocket = accept(ServerSocket, (sockaddr*)&their_addr, &addr_size);
	//	// if (acceptSocket != INVALID_SOCKET)
	//	// {
	//	// 	cout << "Connection acceptee De : " << inet_ntoa(their_addr.sin_addr) << ":" << their_addr.sin_port << "." << endl;
	//	// 
	//	// 	char* buf = new char[6];
	//	// 	if (recv(acceptSocket, buf, sizeof(buf), 0) != SOCKET_ERROR)
	//	// 		msgHandler(buf, acceptSocket);
	//	// 	else printf("recv failed\n", 5000);
	//	// }
	//	// closesocket(acceptSocket);
	//}

	//// No longer need server socket
	//cleanup(ServerSockets, 50);
	return 0;
}
