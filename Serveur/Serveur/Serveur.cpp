#undef UNICODE

#include <winsock2.h>
#include <mutex>
#include <ws2tcpip.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <strstream>
#include <sstream>
#include <windows.h>
#include <ctime>
#include <iomanip>
#include <thread>

// link with Ws2_32.lib
#pragma comment( lib, "ws2_32.lib" )

// External functions
extern DWORD WINAPI EchoHandler(void* sd_);
extern void DoSomething(char *src, char *dest);

// List of Winsock error constants mapped to an interpretation string.
// Note that this list must remain sorted by the error constants'
// values, because we do a binary search on the list when looking up
// items.
static struct ErrorEntry 
{
	int nID;
	const char* pcMessage;

	ErrorEntry(int id, const char* pc = 0) :
		nID(id),
		pcMessage(pc)
	{
	}

	bool operator<(const ErrorEntry& rhs) const
	{
		return nID < rhs.nID;
	}
} 
gaErrorList[] = {
	ErrorEntry(0, "No error"),
	ErrorEntry(WSAEINTR, "Interrupted system call"),
	ErrorEntry(WSAEBADF, "Bad file number"),
	ErrorEntry(WSAEACCES, "Permission denied"),
	ErrorEntry(WSAEFAULT, "Bad address"),
	ErrorEntry(WSAEINVAL, "Invalid argument"),
	ErrorEntry(WSAEMFILE, "Too many open sockets"),
	ErrorEntry(WSAEWOULDBLOCK, "Operation would block"),
	ErrorEntry(WSAEINPROGRESS, "Operation now in progress"),
	ErrorEntry(WSAEALREADY, "Operation already in progress"),
	ErrorEntry(WSAENOTSOCK, "Socket operation on non-socket"),
	ErrorEntry(WSAEDESTADDRREQ, "Destination address required"),
	ErrorEntry(WSAEMSGSIZE, "Message too long"),
	ErrorEntry(WSAEPROTOTYPE, "Protocol wrong type for socket"),
	ErrorEntry(WSAENOPROTOOPT, "Bad protocol option"),
	ErrorEntry(WSAEPROTONOSUPPORT, "Protocol not supported"),
	ErrorEntry(WSAESOCKTNOSUPPORT, "Socket type not supported"),
	ErrorEntry(WSAEOPNOTSUPP, "Operation not supported on socket"),
	ErrorEntry(WSAEPFNOSUPPORT, "Protocol family not supported"),
	ErrorEntry(WSAEAFNOSUPPORT, "Address family not supported"),
	ErrorEntry(WSAEADDRINUSE, "Address already in use"),
	ErrorEntry(WSAEADDRNOTAVAIL, "Can't assign requested address"),
	ErrorEntry(WSAENETDOWN, "Network is down"),
	ErrorEntry(WSAENETUNREACH, "Network is unreachable"),
	ErrorEntry(WSAENETRESET, "Net connection reset"),
	ErrorEntry(WSAECONNABORTED, "Software caused connection abort"),
	ErrorEntry(WSAECONNRESET, "Connection reset by peer"),
	ErrorEntry(WSAENOBUFS, "No buffer space available"),
	ErrorEntry(WSAEISCONN, "Socket is already connected"),
	ErrorEntry(WSAENOTCONN, "Socket is not connected"),
	ErrorEntry(WSAESHUTDOWN, "Can't send after socket shutdown"),
	ErrorEntry(WSAETOOMANYREFS, "Too many references, can't splice"),
	ErrorEntry(WSAETIMEDOUT, "Connection timed out"),
	ErrorEntry(WSAECONNREFUSED, "Connection refused"),
	ErrorEntry(WSAELOOP, "Too many levels of symbolic links"),
	ErrorEntry(WSAENAMETOOLONG, "File name too long"),
	ErrorEntry(WSAEHOSTDOWN, "Host is down"),
	ErrorEntry(WSAEHOSTUNREACH, "No route to host"),
	ErrorEntry(WSAENOTEMPTY, "Directory not empty"),
	ErrorEntry(WSAEPROCLIM, "Too many processes"),
	ErrorEntry(WSAEUSERS, "Too many users"),
	ErrorEntry(WSAEDQUOT, "Disc quota exceeded"),
	ErrorEntry(WSAESTALE, "Stale NFS file handle"),
	ErrorEntry(WSAEREMOTE, "Too many levels of remote in path"),
	ErrorEntry(WSASYSNOTREADY, "Network system is unavailable"),
	ErrorEntry(WSAVERNOTSUPPORTED, "Winsock version out of range"),
	ErrorEntry(WSANOTINITIALISED, "WSAStartup not yet called"),
	ErrorEntry(WSAEDISCON, "Graceful shutdown in progress"),
	ErrorEntry(WSAHOST_NOT_FOUND, "Host not found"),
	ErrorEntry(WSANO_DATA, "No host data of that type was found")
};
const int kNumMessages = sizeof(gaErrorList) / sizeof(ErrorEntry);


//// WSAGetLastErrorMessage ////////////////////////////////////////////
// A function similar in spirit to Unix's perror() that tacks a canned 
// interpretation of the value of WSAGetLastError() onto the end of a
// passed string, separated by a ": ".  Generally, you should implement
// smarter error handling than this, but for default cases and simple
// programs, this function is sufficient.
//
// This function returns a pointer to an internal static buffer, so you
// must copy the data from this function before you call it again.  It
// follows that this function is also not thread-safe.
const char* WSAGetLastErrorMessage(const char* pcMessagePrefix, int nErrorID = 0)
{
	// Build basic error string
	static char acErrorBuffer[256];
	std::ostrstream outs(acErrorBuffer, sizeof(acErrorBuffer));
	outs << pcMessagePrefix << ": ";

	// Tack appropriate canned message onto end of supplied message 
	// prefix. Note that we do a binary search here: gaErrorList must be
	// sorted by the error constant's value.
	ErrorEntry* pEnd = gaErrorList + kNumMessages;
	ErrorEntry Target(nErrorID ? nErrorID : WSAGetLastError());
	ErrorEntry* it = std::lower_bound(gaErrorList, pEnd, Target);
	if ((it != pEnd) && (it->nID == Target.nID)) {
		outs << it->pcMessage;
	}
	else {
		// Didn't find error in list, so make up a generic one
		outs << "unknown error";
	}
	outs << " (" << Target.nID << ")";

	// Finish error message off and return it.
	outs << std::ends;
	acErrorBuffer[sizeof(acErrorBuffer)-1] = '\0';
	return acErrorBuffer;
}

#define PORT 5000

static addrinfo hints;
static std::ofstream myfile;
static bool serverActive = false;
static std::vector<std::string> candidats;
static std::vector<int> votes;
static std::mutex mut;
static SOCKET ServerSocket;
static int FDMax;
static fd_set readFDs, allFDs;
static std::vector<std::string> voters;

void ProcessMsg(char * buf, SOCKET sd)
{
	std::cout << "buf = " << buf << std::endl;

	if (strcmp(buf, "REQ") == 0)
	{
		// We send two messages - one for the size of the name table and one for the table itself
		for (int i = 0; i < candidats.size(); i++)
		{
			std::string msg = std::to_string(i + 1) + "_" + candidats[i];

			if (send(sd, msg.c_str(), 128, 0) == SOCKET_ERROR)
				std::cout << "Envoie de " << msg << " �chou�" << std::endl;
			else std::cout << msg << " envoy�" << std::endl;
		}
		if (send(sd, "0", 128, 0) == SOCKET_ERROR)
			std::cout << "Envoie de 0 �chou�" << std::endl;
	}
	else
	{
		int index;
		sscanf_s(buf, "%d", &index);

		if (index - 1 >= 0 && index - 1 < candidats.size())
		{
			{
				std::lock_guard<std::mutex> lk(mut);
				votes[index - 1] += 1;
			}
			// mut.lock();		// Lock access to votes table
			
			
			// mut.unlock();	// Unlock access to votes table

			std::cout << candidats[index - 1] << " " << votes[index - 1] << std::endl;

			// write time
			myfile << candidats[index - 1];

			std::stringstream buffer;
			const time_t t_now = std::time(0);
			buffer << std::put_time(std::localtime(&t_now), "%Y-%m-%d %H:%M:%S");

			myfile << " " << buffer.str() << std::endl;
		}
	}

	std::cout << "end process msg" << std::endl;
}
void Accept(int fd) 
{
	// std::cout << "ACCEPTING " << fd << std::endl;
	if (fd == ServerSocket) // Nouvelle connection
	{
		sockaddr_in their_addr;
		socklen_t addr_size = sizeof(sockaddr);
		SOCKET acceptSocket = accept(ServerSocket, (sockaddr*)&their_addr, &addr_size);
		if (acceptSocket != INVALID_SOCKET)
		{
			FD_SET(acceptSocket, &allFDs);
			if (acceptSocket > FDMax)
				FDMax = acceptSocket;

			printf("Nouvelle connection port %d\n", PORT);

			const time_t t_now = std::time(0);

			std::stringstream buffer;
			buffer << std::put_time(std::localtime(&t_now), "%Y-%m-%d %H:%M:%S");

			std::string info = buffer.str() + " - " + std::string(inet_ntoa(their_addr.sin_addr)) + ":" + std::to_string(PORT);
			voters.push_back(info);

			int nbBytes;
			char* buf = new char[3];
			if (nbBytes = recv(acceptSocket, buf, sizeof(buf), 0) != SOCKET_ERROR) 
			{
				if (nbBytes == 0)
					printf("Socket %d ferm� par client", acceptSocket);
				else 
					ProcessMsg(buf, acceptSocket);
			}
			else printf("recv failed %d\n", PORT);

			closesocket(acceptSocket);
			FD_CLR(acceptSocket, &allFDs);
		}
	}

	std::cout << "End Accept " << std::endl;
}
void Timer(int duration)
{
	std::printf("Starting timer : %d seconds \n", duration);

	serverActive = true;

	Sleep(duration * 1000);

	serverActive = false;

	std::printf("Timer ended. \n");
}

int main(void)
{
	#pragma region Charger Candidats
	// Charger la liste de candidats ligne par ligne
	std::ifstream in("candidats.txt");
	std::string line;
	while (getline(in, line))
	{
		candidats.push_back(line);
		votes.push_back(0);

		std::cout << line << std::endl;
	}

	// Fermer le fichier - on a charger tout les candidats
	in.close();

	// Sanity check - il faut au moins 1 candidat
	if (candidats.empty())
	{
		printf("candidats.txt est introuvable \n");
		return 1;
	}
	else
	{
		printf("%i candidats trouv�s \n", candidats.size());
	}
	#pragma endregion

	#pragma region Charger Journal
	myfile.open("journal.txt", std::ofstream::out | std::ofstream::trunc);
	#pragma endregion

	// Setup hints
	std::memset(&hints, 0, sizeof(hints));	// empty struct
	hints.ai_family = AF_INET;			// IPv4
	hints.ai_socktype = SOCK_STREAM;	// TCP
	hints.ai_flags = AI_PASSIVE;		// auto fill ip

	#pragma region Temps
	int duration;
	timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;

	std::printf("Entrez la duree du vote (en secondes) ");
	std::cin >> duration;

	std::thread timeThread(Timer, duration);
	timeThread.detach();
	#pragma endregion

	#pragma region Init Winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) 
	{
		std::cerr << "Error at WSAStartup()\n" << std::endl;
		return 1;
	}
	#pragma endregion

	FD_ZERO(&readFDs);
	FD_ZERO(&allFDs);

	// Socket initial
	ServerSocket = socket(hints.ai_family, hints.ai_socktype, IPPROTO_TCP);
	printf("Ouverture du port %d \n", PORT);

	if (ServerSocket != INVALID_SOCKET)	// Si Socket valide...
	{
		addrinfo * myInfo;	// Information d'addresse du serveur
		if (getaddrinfo(NULL, std::to_string(PORT).c_str(), &hints, &myInfo) == 0)	// On va chercher l'info
		{
			std::cout << "Address " << myInfo->ai_addr << std::endl;
			if (bind(ServerSocket, myInfo->ai_addr, myInfo->ai_addrlen) != SOCKET_ERROR)	// Bind
			{
				if (listen(ServerSocket, 30) != SOCKET_ERROR) // On �coute sur le socket
				{
					FD_SET(ServerSocket, &allFDs);	// On change tout les FD
					FDMax = ServerSocket;			// On change le nombre max de FD

					while (serverActive) 
					{
						readFDs = allFDs;
						if (select(FDMax + 1, &readFDs, NULL, NULL, &timeout) != SOCKET_ERROR)
						{
							for (int i = 0; i <= FDMax; i++)
							{
								if (FD_ISSET(i, &readFDs)) 
								{
									std::thread accThread(Accept, i);
									accThread.detach();
								}
							}
						}
						else printf("select error \n");
					}
				}
				else printf("listen error with port %d\n", PORT);
			}
			else printf("bind error with port %d\n", PORT);

			freeaddrinfo(myInfo);	// Lib�rer mon info
		}
		else printf("getaddrinfo error with port %d\n", PORT);

		closesocket(ServerSocket);
	}

	printf("\n Resultats \n");
	for (int i = 0; i < candidats.size(); i++) 
	{
		std::cout << candidats[i] << " : " << votes[i] << std::endl;
	}

	//Write VoteCount
	// std::string voteCountName = "voteResults.txt";
	// std::ofstream voteCount;
	// voteCount.open(voteCountName);
	// for (int i = 0; i < participants.size(); i++)
	// {
	// 	voteCount << participants[i] << " : " << votes[i] << std::endl;
	// }
	// voteCount.close();

	///Write userLog
	// std::string userLogName = "userLog.txt";
	// std::ofstream userLog;
	// userLog.open(userLogName, std::ios::app);
	// for (int i = 0; i < voters.size(); i++)
	// {
	// 	userLog << voters.at(i) << std::endl;
	// }
	// userLog.close();


	myfile.close();
	WSACleanup();
	getchar();
	return 0;
}
